from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from StudentsModels import db, studentmodels


app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:postgres@localhost:5432/postgres"
app.debug = True
db = SQLAlchemy(app)


@app.route('/', methods=['GET'])
def test():
    return {
        'test': 'All good, server is up & running'
    }

@app.route('/studentdetails', methods=['GET'])
def getstudent():
    studentdetails = studentmodels.query.all()
    output = []
    for sdetails in studentdetails:
        currStudent = {}
        currStudent['studentId'] = sdetails.studentId
        currStudent['firstName'] = sdetails.firstName
        currStudent['lastName'] = sdetails.lastName
        currStudent['gender'] = sdetails.gender
        currStudent['mobile'] = sdetails.mobile
        currStudent['qualification'] = sdetails.qualification
        currStudent['collegeName'] = sdetails.collegeName
        output.append(currStudent)
        return jsonify(output)

@app.route('/addstudent', methods=['POST'])
def addstudent():
    addStudent = request.get_json()
    student = studentmodels(studentId=addStudent['studentId'], firstName=addStudent['firstName'], lastName=addStudent['lastName'], gender=addStudent['gender'], mobile=addStudent['mobile'], qualification=addStudent['qualification'], collegeName=addStudent['collegeName'])
    db.session.add(student)
    db.session.commit()
    return jsonify(addStudent)


@app.route('/studentdetailsbyid/<string:studentId>', methods=['GET'])
def search(studentId):
    studentdetails = studentmodels.query.filter(studentmodels.studentId.contains(studentId)).order_by(studentmodels.studentId)
    output = []
    for sdetails in studentdetails:
        currStudent = {}
        currStudent['studentId'] = sdetails.studentId
        currStudent['firstName'] = sdetails.firstName
        currStudent['lastName'] = sdetails.lastName
        currStudent['gender'] = sdetails.gender
        currStudent['mobile'] = sdetails.mobile
        currStudent['qualification'] = sdetails.qualification
        currStudent['collegeName'] = sdetails.collegeName
        output.append(currStudent)
        return jsonify(output)


if __name__ == '__main__':
    app.run(debug=True)




